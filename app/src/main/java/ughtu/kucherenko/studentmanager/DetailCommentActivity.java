package ughtu.kucherenko.studentmanager;

import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import ughtu.kucherenko.studentmanager.fragments.DetailCommentFragment;
import ughtu.kucherenko.studentmanager.providers.LessonContract;

/**
 * Created by kucherenko on 20.02.15.
 */
public class DetailCommentActivity extends ToolBarActivity {

    public static final String TAG = DetailCommentActivity.class.getSimpleName();

    private DetailCommentFragment detailCommentFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if(savedInstanceState == null) {
            final int defaultValue = 0;
            detailCommentFragment = DetailCommentFragment.newInstance(getIntent().getIntExtra(LessonContract.COMMENT, defaultValue));
            startFragment(detailCommentFragment);
        }

        setToolBarTitle(R.string.comment);
        toolBar.setNavigationIcon(R.drawable.icon_white_left_arrow);

    }


    public static Intent newInstance(Context context, int id) {
        Intent intent = new Intent(context, DetailCommentActivity.class);
        intent.putExtra(LessonContract.COMMENT, id);
        return intent;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.detail_comment_menu,menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch(item.getItemId()) {
            case R.id.action_delete:
                if(detailCommentFragment == null) return false;
                Uri uri = ContentUris.withAppendedId(LessonContract.CONTENT_URI, detailCommentFragment.getCommentId());
                ContentValues values = new ContentValues();
                values.put(LessonContract.COMMENT, "");
                int count = getContentResolver().update(uri, values, null, null);
                Toast.makeText(this, R.string.deleted, Toast.LENGTH_LONG).show();
                break;

        }

        return super.onOptionsItemSelected(item);
    }
}
