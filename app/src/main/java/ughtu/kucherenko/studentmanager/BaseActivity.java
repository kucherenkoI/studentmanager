package ughtu.kucherenko.studentmanager;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.ActionBarActivity;
import android.view.MenuItem;

/**
 * Created by kucherenko on 16.02.15.
 */
public class BaseActivity extends ActionBarActivity {

    public void startFragment(Fragment fragment) {
        getSupportFragmentManager().popBackStack();
        final FragmentTransaction mFragmentTransaction = getSupportFragmentManager()
                .beginTransaction();
        mFragmentTransaction.replace(R.id.fragment_container, fragment, fragment.getClass().getSimpleName());
        mFragmentTransaction.commit();
        getSupportFragmentManager().executePendingTransactions();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

}
