package ughtu.kucherenko.studentmanager.adapter;

import android.content.Context;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.support.v4.view.PagerAdapter;
import android.view.View;
import android.view.ViewGroup;

import java.util.HashSet;
import java.util.Set;

import ughtu.kucherenko.studentmanager.MainActivity;
import ughtu.kucherenko.studentmanager.R;
import ughtu.kucherenko.studentmanager.pojo.Lesson;
import ughtu.kucherenko.studentmanager.providers.DateProvider;
import ughtu.kucherenko.studentmanager.providers.LessonContract;
import ughtu.kucherenko.studentmanager.providers.Weak;
import ughtu.kucherenko.studentmanager.view.DayListHeaderView;
import ughtu.kucherenko.studentmanager.view.DayListView;
import ughtu.kucherenko.studentmanager.view.EmptyDayView;

/**
 * Created by kucherenko on 16.02.15.
 */
public class TimeTablePagerAdapter extends PagerAdapter {

    protected Cursor mCursor;
    protected Context mContext;
    private Set<Integer> set = new HashSet<>();

    public TimeTablePagerAdapter(Context context) {
        this.mContext = context;
    }

    @Override
    public int getCount() {
        return set == null ? 0 : set.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object o) {
        return view == o;
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {

        if(isDayEmpty(position)) {
            EmptyDayView emptyDayView = new EmptyDayView(mContext);
            container.addView(emptyDayView);
            return emptyDayView;
        }

        DayListView dayListView = new DayListView(mContext);
        DayListAdapter dayListAdapter = new DayListAdapter(mContext);
        dayListAdapter.setWeak(position);
        dayListView.addHeaderView(getHeaderViewById(position), null,false);
        dayListView.setAdapter(dayListAdapter);
        container.addView(dayListView);
        return dayListView;
    }

    public void setCursor(Cursor cursor) {
        mCursor = cursor;
        while (mCursor.moveToNext()) {
            set.add(mCursor.getInt(mCursor.getColumnIndex(LessonContract.DAY_OF_WEAK)));
        }
        notifyDataSetChanged();
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((View) object);
    }

    protected DayListHeaderView getHeaderViewById(int position) {
        DayListHeaderView headerView = new DayListHeaderView(mContext);
        switch(position) {
            case 0:
                headerView.setDate(DateProvider.getFormatDate(0, Weak.MONDAY));
                headerView.setDayOfWeak(mContext.getString(R.string.monday));
                break;
            case 1:
                headerView.setDate(DateProvider.getFormatDate(0, Weak.TUESDAY));
                headerView.setDayOfWeak(mContext.getString(R.string.tuesday));
                break;
            case 2:
                headerView.setDate(DateProvider.getFormatDate(0, Weak.WEDNESDAY));
                headerView.setDayOfWeak(mContext.getString(R.string.wednesday));
                break;
            case 3:
                headerView.setDate(DateProvider.getFormatDate(0, Weak.THURSDAY));
                headerView.setDayOfWeak(mContext.getString(R.string.thursday));
                break;
            case 4:
                headerView.setDate(DateProvider.getFormatDate(0, Weak.FRIDAY));
                headerView.setDayOfWeak(mContext.getString(R.string.friday));
                break;
            case 5:
                headerView.setDate(DateProvider.getFormatDate(0, Weak.SATURDAY));
                headerView.setDayOfWeak(mContext.getString(R.string.saturday));
                break;
            case 6:
                headerView.setDate(DateProvider.getFormatDate(0, Weak.SUNDAY));
                headerView.setDayOfWeak(mContext.getString(R.string.sunday));
                break;
        }
        return headerView;
    }

    public boolean isDayEmpty(int position) {
        SharedPreferences sharedPreferences = mContext.getSharedPreferences(MainActivity.TAG, Context.MODE_PRIVATE);
        int typeOfWeak = sharedPreferences.getInt(LessonContract.TYPE_OF_WEAK, Lesson.EVERY);

        Cursor cursor = mContext.getContentResolver().query(LessonContract.CONTENT_URI,
                null,
                LessonContract.DAY_OF_WEAK + " = ? AND " + LessonContract.TYPE_OF_WEAK + " = ? OR " + LessonContract.DAY_OF_WEAK + " = ? AND " + LessonContract.TYPE_OF_WEAK + " = ?",
                new String[]{String.valueOf(position), String.valueOf(typeOfWeak), String.valueOf(position), String.valueOf(Lesson.EVERY)},
                null);
        if(cursor.moveToFirst()) {
            return false;
        } else {
            return true;
        }

    }

}
