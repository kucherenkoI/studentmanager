package ughtu.kucherenko.studentmanager.adapter;

import android.content.Context;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;

import ughtu.kucherenko.studentmanager.MainActivity;
import ughtu.kucherenko.studentmanager.R;
import ughtu.kucherenko.studentmanager.pojo.Lesson;
import ughtu.kucherenko.studentmanager.providers.LessonContract;
import ughtu.kucherenko.studentmanager.view.LessonItemView;

/**
 * Created by kucherenko on 16.02.15.
 */
public class DayListAdapter extends ArrayAdapter<Cursor> {

    protected Cursor mCursor;

    public DayListAdapter(Context context) {
        super(context, R.layout.lesson_item_view);
    }

    @Override
    public int getCount() {
        return mCursor == null ? 0 : mCursor.getCount();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        mCursor.moveToPosition(position);
        LessonItemView lessonItemView = new LessonItemView(getContext(), mCursor);
        return lessonItemView;
    }

    public void setWeak(int dayOfWeak) {

        SharedPreferences sharedPreferences = getContext().getSharedPreferences(MainActivity.TAG, Context.MODE_PRIVATE);
        int typeOfWeak = sharedPreferences.getInt(LessonContract.TYPE_OF_WEAK, Lesson.EVERY);

            mCursor = getContext().getContentResolver().query(LessonContract.CONTENT_URI,
                    null,
                    LessonContract.DAY_OF_WEAK + " = ? AND " + LessonContract.TYPE_OF_WEAK + " = ? OR " + LessonContract.DAY_OF_WEAK + " = ? AND " + LessonContract.TYPE_OF_WEAK + " = ?",
                    new String[]{String.valueOf(dayOfWeak), String.valueOf(typeOfWeak), String.valueOf(dayOfWeak), String.valueOf(Lesson.EVERY)},
                    null);
        notifyDataSetChanged();
    }


}
