package ughtu.kucherenko.studentmanager.adapter;

import android.content.Context;
import android.database.Cursor;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;

import ughtu.kucherenko.studentmanager.R;
import ughtu.kucherenko.studentmanager.providers.LessonContract;
import ughtu.kucherenko.studentmanager.view.CommentItemView;

/**
 * Created by kucherenko on 20.02.15.
 */
public class CommentListAdapter extends ArrayAdapter {

    protected Cursor mCursor;

    public CommentListAdapter(Context context) {
        super(context, R.layout.comment_item_view);
    }

    public void setCursor(Cursor cursor) {
        mCursor = cursor;
        notifyDataSetChanged();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        CommentItemView itemView = new CommentItemView(getContext());
        itemView.setDescriptionText(mCursor.getString(mCursor.getColumnIndex(LessonContract.COMMENT)));
        itemView.setLessonText(mCursor.getString(mCursor.getColumnIndex(LessonContract.NAME_OF_LESSON)));
        itemView.setId(mCursor.getInt(mCursor.getColumnIndex(LessonContract._ID)));
        return itemView;
    }

    @Override
    public int getCount() {
        return mCursor == null ? 0 : mCursor.getCount();
    }
}
