package ughtu.kucherenko.studentmanager.adapter;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;

import ughtu.kucherenko.studentmanager.R;
import ughtu.kucherenko.studentmanager.view.LoadListItemView;

/**
 * Created by kucherenko on 18.01.15.
 */
public class LoadListAdapter extends ArrayAdapter<String> {

    protected static final int ZERO = 0;

    protected String[] mData;

    protected LoadListItemView itemView;

    public LoadListAdapter(Context context) {
        super(context, R.layout.load_list_item_view);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        itemView = new LoadListItemView(getContext());
        itemView.setText(mData[position]);


        return itemView;
    }

    public void setTextSize(float size) {
        itemView.setTextSize(size);
    }

    public void setData(String[] data) {
        mData = data;
        notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        return mData == null ? ZERO : mData.length;
    }
}
