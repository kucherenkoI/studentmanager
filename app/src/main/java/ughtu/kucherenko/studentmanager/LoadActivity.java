package ughtu.kucherenko.studentmanager;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import ughtu.kucherenko.studentmanager.fragments.LoadFragment;
import ughtu.kucherenko.studentmanager.providers.LessonContract;

import static android.support.v7.widget.Toolbar.OnMenuItemClickListener;


public class LoadActivity extends ToolBarActivity {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if(!isLessonDBEmpty()) {
            startActivity(MainActivity.newInstance(this));
            finish();
            return;
        }

        if(savedInstanceState == null) {
            startFragment(LoadFragment.newInstance());
            toolBar.setOnMenuItemClickListener(onMenuItemClickListener);
        }
    }

    protected boolean isLessonDBEmpty() {
        Cursor cursor = getContentResolver().query(LessonContract.CONTENT_URI,
                null,
                null,
                null,
                null);
        final int zero = 0;
        if(cursor == null) {
            cursor.close();
            return true;
        }

        boolean isEmpty = cursor.getCount() == zero;
        cursor.close();
        return isEmpty;

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_load, menu);
        return super.onCreateOptionsMenu(menu);
    }

    private OnMenuItemClickListener onMenuItemClickListener = new OnMenuItemClickListener() {
        @Override
        public boolean onMenuItemClick(MenuItem item) {

            switch (item.getItemId()) {
                case R.id.action_mail:
                    showMailMessage();
                    return true;
                case R.id.action_next:
                    LoadActivity.this.finish();
                    startActivity(MainActivity.newInstance(getApplicationContext()));
                    return true;
            }
            return false;
        }
    };

    private void showMailMessage() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage(R.string.mail_message);
        builder.setPositiveButton(R.string.send_author, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {


                Intent sendIntent = new Intent(Intent.ACTION_VIEW);
                sendIntent.setType("plain/text");
                sendIntent.setData(Uri.parse("kuchernkoigor@gmail.com"));
                sendIntent.setClassName("com.google.android.gm", "com.google.android.gm.ComposeActivityGmail");
                sendIntent.putExtra(Intent.EXTRA_EMAIL, new String[]{"kuchernkoigor@gmail.com"});
                sendIntent.putExtra(Intent.EXTRA_SUBJECT, "StudentManager");
                sendIntent.putExtra(Intent.EXTRA_TEXT, "");

                if (sendIntent.resolveActivity(getPackageManager()) != null) {
                    startActivity(sendIntent);
                } else {
                    Toast.makeText(LoadActivity.this, R.string.no_gmail, Toast.LENGTH_LONG).show();
                }

            }
        });
        builder.setNegativeButton(android.R.string.cancel, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        builder.show();
    }
}
