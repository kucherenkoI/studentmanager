package ughtu.kucherenko.studentmanager.loaders;

import android.annotation.TargetApi;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Build;

import com.google.gson.Gson;
import com.google.gson.stream.JsonReader;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;

import ughtu.kucherenko.studentmanager.MainActivity;
import ughtu.kucherenko.studentmanager.pojo.Weak;
import ughtu.kucherenko.studentmanager.providers.LessonContract;

/**
 * Created by kucherenko on 24.02.15.
 */
public class LessonLoader extends AsyncTask<Void, Void, Void> {


    private Context mContext;
    private String mPath;


    public LessonLoader(Context mContext,String path) {
        this.mContext = mContext;
        this.mPath = path;
    }

    @Override
    protected Void doInBackground(Void... params) {



        try {

            URL website = new URL(mPath);
            URLConnection ucon = website.openConnection();
            InputStream is = ucon.getInputStream();
            Gson gson = new Gson();
            InputStreamReader isr = new InputStreamReader(is);

            JsonReader reader = new JsonReader(isr);

            Weak weak = gson.fromJson(reader, Weak.class);

            mContext.getContentResolver().bulkInsert(LessonContract.CONTENT_URI, weak.toContentValues());


        } catch(IOException e) {
            e.printStackTrace();
        }


        return null;
    }


    @Override
    protected void onPostExecute(Void aVoid) {
        super.onPostExecute(aVoid);

        mContext.startActivity(MainActivity.newInstance(mContext));
    }

    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    public void start() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR1 ) {
            executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
        } else {
            execute(new Void[]{});
        }
    }

}
