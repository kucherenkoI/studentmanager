package ughtu.kucherenko.studentmanager;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.widget.TextView;

import butterknife.ButterKnife;
import butterknife.InjectView;

/**
 * Created by kucherenko on 17.01.15.
 */
public class ToolBarActivity extends BaseActivity {

    @InjectView(R.id.toolbar)
    protected Toolbar toolBar;
    @InjectView(R.id.toolbar_title)
    protected TextView toolBarTitle;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fragment_container);
        ButterKnife.inject(this);
        setSupportActionBar(toolBar);
    }

    public void setToolBarBackground(int color) {
        toolBar.setBackgroundColor(getResources().getColor(color));
    }

    public void setToolBarTitle(int res) {
        toolBarTitle.setText(res);
    }


}
