package ughtu.kucherenko.studentmanager.view;

import android.content.Context;
import android.widget.LinearLayout;
import android.widget.TextView;

import butterknife.ButterKnife;
import butterknife.InjectView;
import ughtu.kucherenko.studentmanager.R;

/**
 * Created by kucherenko on 18.02.15.
 */
public class DayListHeaderView extends LinearLayout {

    @InjectView(R.id.day_of_weak)
    protected TextView dayOfWeakView;
    @InjectView(R.id.date)
    protected TextView dateView;


    public DayListHeaderView(Context context) {
        super(context);
        inflate(context, R.layout.day_list_header_view, this);
        ButterKnife.inject(this);
    }

    public void setDayOfWeak(String dayOfWeak) {
        dayOfWeakView.setText(dayOfWeak);
    }

    public void setDate(String date) {
        dateView.setText(date);
    }

}
