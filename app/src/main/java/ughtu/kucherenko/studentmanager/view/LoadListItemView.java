package ughtu.kucherenko.studentmanager.view;

import android.content.Context;
import android.widget.LinearLayout;
import android.widget.TextView;

import butterknife.ButterKnife;
import butterknife.InjectView;
import ughtu.kucherenko.studentmanager.R;

/**
 * Created by kucherenko on 18.01.15.
 */
public class LoadListItemView extends LinearLayout {

    @InjectView(R.id.load_list_item_content)
    protected TextView contentTextView;

    public LoadListItemView(Context context) {
        super(context);
        inflate(context, R.layout.load_list_item_view, this);
        ButterKnife.inject(this);
    }

    public void setText(int res) {
        contentTextView.setText(res);
    }

    public void setText(String text) {
        contentTextView.setText(text);
    }

    public void setTextSize(float size) {
        contentTextView.setTextSize(size);
    }

}
