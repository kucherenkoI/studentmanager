package ughtu.kucherenko.studentmanager.view;

import android.content.Context;
import android.widget.LinearLayout;

import ughtu.kucherenko.studentmanager.R;

/**
 * Created by kucherenko on 24.02.15.
 */
public class EmptyDayView extends LinearLayout {
    public EmptyDayView(Context context) {
        super(context);
        inflate(context, R.layout.empty_day_view, this);
    }
}
