package ughtu.kucherenko.studentmanager.view;

import android.app.AlertDialog;
import android.content.ContentUris;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.Toast;

import com.tjeannin.provigen.annotation.ContentUri;

import ughtu.kucherenko.studentmanager.CreateLessonActivity;
import ughtu.kucherenko.studentmanager.EditLessonActivity;
import ughtu.kucherenko.studentmanager.MainActivity;
import ughtu.kucherenko.studentmanager.R;
import ughtu.kucherenko.studentmanager.adapter.DayListAdapter;
import ughtu.kucherenko.studentmanager.adapter.TimeTablePagerAdapter;
import ughtu.kucherenko.studentmanager.fragments.TimeTableFragment;
import ughtu.kucherenko.studentmanager.providers.LessonContract;

/**
 * Created by kucherenko on 16.02.15.
 */
public class DayListView extends ListView {

    public DayListView(Context context) {
        super(context);
        setOnItemClickListener(onItemClickListener);
    }

    protected OnItemClickListener onItemClickListener = new OnItemClickListener() {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            LessonItemView lessonItemView = (LessonItemView)view;
            showDialog(lessonItemView.getId());
        }
    };

    protected void showDialog(final int id) {

        final ArrayAdapter adapter = new ArrayAdapter<String>(
                getContext(), android.R.layout.select_dialog_item);
        adapter.add(getContext().getString(R.string.create_lesson));
        adapter.add(getContext().getString(R.string.edit_lesson));
        adapter.add(getContext().getString(R.string.delete));
        adapter.add(getContext().getString(android.R.string.cancel));

        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());

        builder.setTitle(R.string.your_action);

        builder.setCancelable(false);

        builder.setAdapter(adapter, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {

                switch(item) {

                    case 0:
                        getContext().startActivity(CreateLessonActivity.newInstance(getContext()));
                        break;
                    case 1:
                        getContext().startActivity(EditLessonActivity.newInstance(getContext(), id));
                        break;
                    case 2:
                        Uri uri = ContentUris.withAppendedId(LessonContract.CONTENT_URI, id);
                        int count = getContext().getContentResolver().delete(uri, null, null);
                        if(count > 0) {
                            Toast.makeText(getContext(), R.string.success, Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(getContext(), R.string.error, Toast.LENGTH_SHORT).show();
                        }

                        if(getContext() instanceof MainActivity) {
                            MainActivity activity = (MainActivity)getContext();
                            activity.startFragment(TimeTableFragment.newInstance());
                        }

                        break;
                    case 3:
                        dialog.cancel();
                        break;

                }
            }
        });

        builder.show();
    }

}
