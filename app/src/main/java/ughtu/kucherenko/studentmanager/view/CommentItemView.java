package ughtu.kucherenko.studentmanager.view;

import android.content.Context;
import android.widget.LinearLayout;
import android.widget.TextView;

import butterknife.ButterKnife;
import butterknife.InjectView;
import ughtu.kucherenko.studentmanager.R;

/**
 * Created by kucherenko on 20.02.15.
 */
public class CommentItemView extends LinearLayout {

    @InjectView(R.id.comment_item_lesson)
    protected TextView lessonText;
    @InjectView(R.id.comment_item_description)
    protected TextView descriptionText;
    protected int id;

    public CommentItemView(Context context) {
        super(context);
        inflate(context, R.layout.comment_item_view, this);
        ButterKnife.inject(this);
    }

    public void setLessonText(String lesson) {
        lessonText.setText(lesson);
    }

    public void setDescriptionText(String description) {
        descriptionText.setText(description);
    }

    @Override
    public int getId() {
        return id;
    }

    @Override
    public void setId(int id) {
        this.id = id;
    }


}
