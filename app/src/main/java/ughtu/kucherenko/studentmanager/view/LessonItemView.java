package ughtu.kucherenko.studentmanager.view;

import android.content.Context;
import android.database.Cursor;
import android.widget.LinearLayout;
import android.widget.TextView;

import butterknife.ButterKnife;
import butterknife.InjectView;
import ughtu.kucherenko.studentmanager.R;
import ughtu.kucherenko.studentmanager.pojo.Lesson;
import ughtu.kucherenko.studentmanager.providers.LessonContract;

/**
 * Created by kucherenko on 31.01.15.
 */
public class LessonItemView extends LinearLayout {

    @InjectView(R.id.number_view)
    protected TextView numberView;
    @InjectView(R.id.lesson_name_view)
    protected TextView lessonNameView;
    @InjectView(R.id.flat_view)
    protected TextView flatView;
    @InjectView(R.id.type_of_weak_view)
    protected TextView typeOfWeakView;
    @InjectView(R.id.name_of_teacher_view)
    protected TextView teacherNameView;
    @InjectView(R.id.building_view)
    protected TextView buildingView;

    protected Cursor mCursor;

    protected int id;


    public LessonItemView(Context context,Cursor cursor) {
        super(context);
        inflate(context, R.layout.lesson_item_view, this);
        ButterKnife.inject(this);
        mCursor = cursor;
        showData();
        setId(mCursor.getInt(mCursor.getColumnIndex(LessonContract._ID)));
    }

    @Override
    public int getId() {
        return id;
    }

    @Override
    public void setId(int id) {
        this.id = id;
    }

    protected void showData() {

        numberView.setText(String.valueOf(mCursor.getInt(mCursor.getColumnIndex(LessonContract.NUMBER))));
        lessonNameView.setText(mCursor.getString(mCursor.getColumnIndex(LessonContract.NAME_OF_LESSON)));
        flatView.setText(mCursor.getString(mCursor.getColumnIndex(LessonContract.FLAT)));
        final int typeOfWeak = mCursor.getInt(mCursor.getColumnIndex(LessonContract.TYPE_OF_WEAK));
        showTypeOfWeak(typeOfWeak);
        teacherNameView.setText(mCursor.getString(mCursor.getColumnIndex(LessonContract.TEACHERS_NAME)));
        buildingView.setText(mCursor.getString(mCursor.getColumnIndex(LessonContract.BUILDING)));

    }

    private void showTypeOfWeak(int typeOfWeak) {
        switch (typeOfWeak) {
            case Lesson.NUMERATOR:
                typeOfWeakView.setText(getContext().getResources().getString(R.string.weak_numerator));
                break;
            case Lesson.DENOMINATOR:
                typeOfWeakView.setText(getContext().getResources().getString(R.string.weak_denumerator));
                break;
            case Lesson.EVERY:
                typeOfWeakView.setText(getContext().getResources().getString(R.string.weak_every));
                break;
            default: typeOfWeakView.setText(getContext().getResources().getString(R.string.weak_every));
        }
    }

}
