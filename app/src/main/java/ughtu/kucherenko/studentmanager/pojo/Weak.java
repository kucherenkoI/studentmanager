package ughtu.kucherenko.studentmanager.pojo;

import android.content.ContentValues;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Igor on 06.09.2014.
 */
public class Weak implements Serializable{

    public static final String FIELD_DAYS = "days";

    @SerializedName(FIELD_DAYS)
    private Day[] days;

    public Day[] getDays() {
        return days;
    }

    public void setDays(Day[] days) {
        this.days = days;
    }

    public ContentValues[] toContentValues() {
        List<ContentValues> list = new ArrayList<>();
        for(Day day: days) {
            list.addAll(day.toContentValues());
        }
        ContentValues[] result = new ContentValues[list.size()];
        return list.toArray(result);
    }

}
