package ughtu.kucherenko.studentmanager.pojo;

import android.content.ContentValues;

import com.google.gson.annotations.SerializedName;

import org.apache.commons.lang3.StringEscapeUtils;

import java.io.Serializable;

import ughtu.kucherenko.studentmanager.providers.LessonContract;

/**
 * Created by Igor on 06.09.2014.
 */
public class Lesson  implements Serializable {

    public static final int NUMERATOR   = 1;
    public static final int DENOMINATOR = 2;
    public static final int EVERY       = 3;

    public static final String FIELD_TYPE_OF_WEAK    = "typeofweak";
    public static final String FIELD_NAME_OF_TEACHER = "nameofteacher";
    public static final String FIELD_NAME_OF_LESSON  = "nameoflesson";
    public static final String FIELD_NUMBER          = "number";
    public static final String FIELD_FLAT            = "flat";
    public static final String FIELD_BUILDING        = "building";

    @SerializedName(FIELD_BUILDING)
    private String typeOfBuilding;
    @SerializedName(FIELD_TYPE_OF_WEAK)
    private Integer typeOfWeak;
    @SerializedName(FIELD_NAME_OF_TEACHER)
    private String nameOfTeacher;
    @SerializedName(FIELD_NAME_OF_LESSON)
    private String nameOfLesson;
    @SerializedName(FIELD_NUMBER)
    private Integer number;
    @SerializedName(FIELD_FLAT)
    private String flat;

    public String getTypeOfBuilding() {
        return typeOfBuilding;
    }

    public void setTypeOfBuilding(String typeOfBuilding) {
        this.typeOfBuilding = typeOfBuilding;
    }

    public Integer getTypeOfWeak() {
        return typeOfWeak;
    }

    public void setTypeOfWeak(Integer typeOfWeak) {
        this.typeOfWeak = typeOfWeak;
    }

    public String getNameOfTeacher() {
        return nameOfTeacher;
    }

    public void setNameOfTeacher(String nameOfTeacher) {
        this.nameOfTeacher = nameOfTeacher;
    }

    public String getNameOfLesson() {
        return nameOfLesson;
    }

    public void setNameOfLesson(String nameOfLesson) {
        this.nameOfLesson = nameOfLesson;
    }

    public Integer getNumber() {
        return number;
    }

    public void setNumber(Integer number) {
        this.number = number;
    }

    public String getFlat() {
        return flat;
    }

    public void setFlat(String flat) {
        this.flat = flat;
    }
    public boolean isEmpty() {
        if(getNumber() == 0 || getNameOfLesson() == null || getNameOfLesson().isEmpty() || getTypeOfWeak() == 0)
            return true;
         else 
            return false;
    }

    public ContentValues toContentValues() {
        ContentValues values = new ContentValues();
        values.put(LessonContract.TYPE_OF_WEAK, getTypeOfWeak());
        values.put(LessonContract.NAME_OF_LESSON, StringEscapeUtils.unescapeJava(getNameOfLesson()));
        values.put(LessonContract.TEACHERS_NAME, StringEscapeUtils.unescapeJava(getNameOfTeacher()));
        values.put(LessonContract.BUILDING,StringEscapeUtils.unescapeJava(getTypeOfBuilding()));
        values.put(LessonContract.FLAT, StringEscapeUtils.unescapeJava(getFlat()));
        values.put(LessonContract.NUMBER,getNumber());
        return values;
    }

}
