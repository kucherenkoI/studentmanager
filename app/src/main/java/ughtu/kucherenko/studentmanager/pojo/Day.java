package ughtu.kucherenko.studentmanager.pojo;

import android.content.ContentValues;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import ughtu.kucherenko.studentmanager.providers.LessonContract;

/**
 * Created by Igor on 06.09.2014.
 */
public class Day  implements Serializable {

    public static final String FIELD_LESSONS = "lessons";
    public static final String FIELD_DAY_OF_WEAK = "dayofweak";
    

    @SerializedName(FIELD_LESSONS)
    private Lesson[] lessons;
    @SerializedName(FIELD_DAY_OF_WEAK)
    private Integer dayOfWeak;

    public Integer getDayOfWeak() {
        return dayOfWeak;
    }

    public void setDayOfWeak(Integer dayOfWeak) {
        this.dayOfWeak = dayOfWeak;
    }

    public Lesson[] getLessons() {
        return lessons;
    }

    public void setLessons(Lesson[] lessons) {
        this.lessons = lessons;
    }

    public List<ContentValues> toContentValues() {
        List<ContentValues> list = new ArrayList<>();
        for(Lesson lesson: lessons) {
            ContentValues values = lesson.toContentValues();
            values.put(LessonContract.DAY_OF_WEAK,getDayOfWeak());

            list.add(values);
        }
        return list;
    }

}
