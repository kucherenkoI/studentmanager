package ughtu.kucherenko.studentmanager.fragments;

import android.content.ContentUris;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import butterknife.ButterKnife;
import butterknife.InjectView;
import ughtu.kucherenko.studentmanager.R;
import ughtu.kucherenko.studentmanager.providers.LessonContract;

/**
 * Created by kucherenko on 24.02.15.
 */
public class DetailCommentFragment extends BaseFragment {


    @InjectView(R.id.detail_comment_name_of_lesson)
    protected TextView nameOfLessonView;
    @InjectView(R.id.detail_comment_text)
    protected TextView commentTextView;

    int commentId = 0;

    public static final String TAG = DetailCommentFragment.class.getSimpleName();

    public int getCommentId() {
        return commentId;
    }

    public static DetailCommentFragment newInstance(int id) {
        final DetailCommentFragment  fragment = new DetailCommentFragment();
        Bundle bundle = new Bundle();
        bundle.putInt(LessonContract.COMMENT, id);
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        commentId = getArguments().getInt(LessonContract.COMMENT);
        Log.d(TAG, LessonContract.COMMENT + " id: " + commentId);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.fragment_detail_comment, container, false);
        ButterKnife.inject(this, view);
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        Cursor cursor = getCursor();

        if(cursor.moveToFirst()) {
            String nameOfLesson = getStringFromCursor(cursor, LessonContract.NAME_OF_LESSON);
            String comment = getStringFromCursor(cursor, LessonContract.COMMENT);
            nameOfLessonView.setText(nameOfLesson);
            commentTextView.setText(comment);
        } else {
            showLongToast(getString(R.string.error));
        }

    }

    private Cursor getCursor() {
        Uri uri = ContentUris.withAppendedId(LessonContract.CONTENT_URI, commentId);
        return getActivity().getContentResolver().query(uri,
                null, null, null, null);
    }

}
