package ughtu.kucherenko.studentmanager.fragments;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.ListView;

import butterknife.ButterKnife;
import butterknife.InjectView;
import ughtu.kucherenko.studentmanager.R;
import ughtu.kucherenko.studentmanager.ToolBarActivity;
import ughtu.kucherenko.studentmanager.adapter.LoadListAdapter;
import ughtu.kucherenko.studentmanager.loaders.LessonLoader;
import ughtu.kucherenko.studentmanager.view.LoadListItemView;

import static android.view.animation.Animation.AnimationListener;
import static android.widget.AdapterView.OnItemClickListener;

/**
 * Created by kucherenko on 18.01.15.
 */
public class LoadFragment extends BaseFragment {

    @InjectView(R.id.load_list_view)
    protected ListView listView;
    protected LoadListAdapter loadListAdapter;
    protected LoadListItemView headerView;

    protected Animation animation;

    private int groupsArrayRes;

    protected static final int MECH_POSITION = 1;
    protected static final int TVMS_POSITION = 2;
    protected static final int TS_POSITION = 3;
    protected static final int TNV_POSITION = 4;
    protected static final int TOV_POSITION = 5;
    protected static final int ECONOM_POSITION = 6;
    protected static final int KN_AND_I_POSITION = 7;

    private String path = "http://studentmanager.ucoz.ua/";

    public static LoadFragment newInstance() {
        final LoadFragment fragment = new LoadFragment();
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.fragment_load, container, false);
        ButterKnife.inject(this, view);
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        animation = AnimationUtils.loadAnimation(getActivity(), android.R.anim.slide_out_right);
        animation.setAnimationListener(animationCoursesListener);

        headerView = new LoadListItemView(getActivity());
        headerView.setText(R.string.choose_your_courses);
        listView.addHeaderView(headerView, null, false);
        listView.setOnItemClickListener(onCoursesItemClickListener);

        loadListAdapter = new LoadListAdapter(getActivity());
        loadListAdapter.setData(getStringArray(R.array.courses));
        listView.setAdapter(loadListAdapter);

    }

    private void setToolBarTitle(int res) {
        Activity activity = getActivity();

        if(activity != null && activity instanceof ToolBarActivity) {
            ((ToolBarActivity)activity).setToolBarTitle(res);
        }
    }

    private OnItemClickListener onCoursesItemClickListener = new OnItemClickListener() {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

            path = path + position + "/";

            view.startAnimation(animation);
        }
    };

    private OnItemClickListener onFacultiesItemClickListener = new OnItemClickListener() {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

            switch(position) {

                case MECH_POSITION: {
                    groupsArrayRes = R.array.faculties_mech;
                    path = path + "mech/";
                    break;
                }
                case TVMS_POSITION: {
                    groupsArrayRes = R.array.faculties_tvms;
                    path = path + "tvms/";
                    break;
                }
                case TS_POSITION: {
                    groupsArrayRes = R.array.faculties_ts;
                    path = path + "ts/";
                    break;
                }
                case TNV_POSITION: {
                    groupsArrayRes = R.array.faculties_tnv;
                    path = path + "tnv/";
                    break;
                }
                case TOV_POSITION: {
                    groupsArrayRes = R.array.faculties_tov;
                    path = path + "tov/";
                    break;
                }
                case ECONOM_POSITION: {
                    groupsArrayRes = R.array.faculties_econom;
                    path = path + "econom/";
                    break;
                }
                case KN_AND_I_POSITION: {
                    groupsArrayRes = R.array.faculties_kn_and_i;
                    path = path + "knii/";
                    break;
                }
            }

            view.startAnimation(animation);

        }
    };

    private OnItemClickListener onGroupsItemClickListener = new OnItemClickListener() {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

            path = path + position + "/shed.txt";

            new LessonLoader(getActivity(), path).start();

            view.startAnimation(animation);

        }
    };


    private AnimationListener animationCoursesListener = new AnimationListener() {
        @Override
        public void onAnimationStart(Animation animation) {

        }

        @Override
        public void onAnimationEnd(Animation animation) {
            final float textSize = getResources().getDimension(R.dimen.facultities_text_size);
            headerView.setTextSize(textSize);
            headerView.setText(R.string.choose_your_faculty);
            loadListAdapter.setTextSize(textSize);
            loadListAdapter.setData(getStringArray(R.array.faculties));
            listView.setOnItemClickListener(onFacultiesItemClickListener);
            animation.setAnimationListener(animationFacultiesListener);
        }

        @Override
        public void onAnimationRepeat(Animation animation) {

        }
    };

    private AnimationListener animationFacultiesListener = new AnimationListener() {
        @Override
        public void onAnimationStart(Animation animation) {

        }

        @Override
        public void onAnimationEnd(Animation animation) {
            final float textSize = getResources().getDimension(R.dimen.load_list_content_size);
            headerView.setText(R.string.choose_your_group);
            loadListAdapter.setTextSize(textSize);
            final int ZERO = 0;
            if(groupsArrayRes != ZERO) {
                loadListAdapter.setData(getStringArray(groupsArrayRes));
            }
            listView.setOnItemClickListener(onGroupsItemClickListener);
        }

        @Override
        public void onAnimationRepeat(Animation animation) {

        }
    };



}
