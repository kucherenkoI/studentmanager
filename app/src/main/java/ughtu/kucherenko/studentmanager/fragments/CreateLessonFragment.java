package ughtu.kucherenko.studentmanager.fragments;

import android.content.ContentValues;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;
import ughtu.kucherenko.studentmanager.R;
import ughtu.kucherenko.studentmanager.pojo.Lesson;
import ughtu.kucherenko.studentmanager.providers.LessonContract;

/**
 * Created by kucherenko on 31.01.15.
 */
public class CreateLessonFragment extends BaseFragment {

    @InjectView(R.id.number_selector)
    protected LinearLayout numberSelectorView;
    @InjectView(R.id.number_edit_view)
    protected TextView numberEditView;
    @InjectView(R.id.teachers_name_edit_field)
    protected EditText teachersNameEditField;
    @InjectView(R.id.auditorium_edit_field)
    protected EditText auditoriumEditField;
    @InjectView(R.id.lesson_edit_field)
    protected EditText lessonEditField;
    @InjectView(R.id.comment_edit_field)
    protected EditText commentEditField;
    @InjectView(R.id.building_content)
    protected LinearLayout buildingContent;
    @InjectView(R.id.building_title)
    protected TextView buildingTitle;
    @InjectView(R.id.weak_content)
    protected LinearLayout weakContent;
    @InjectView(R.id.weak_title)
    protected TextView weakTitle;
    @InjectView(R.id.day_edit_view)
    protected TextView dayEditView;
    @InjectView(R.id.number_selector_day)
    protected LinearLayout dayNumberSelector;

    protected ContentValues contentValues = new ContentValues();


    public static CreateLessonFragment newInstance() {
        final CreateLessonFragment fragment = new CreateLessonFragment();
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.fragment_create, container, false);
        ButterKnife.inject(this, view);
        return view;
    }

    @OnClick(R.id.number_edit_view)
    public void onNumberEditClick() {
        if(numberSelectorView.getVisibility() == View.GONE)
            numberSelectorView.setVisibility(View.VISIBLE);
        else
            numberSelectorView.setVisibility(View.GONE);
    }

    @OnClick(R.id.day_edit_view)
    public void onDayNumberEditClick() {
        if(dayNumberSelector.getVisibility() == View.GONE)
            dayNumberSelector.setVisibility(View.VISIBLE);
        else
            dayNumberSelector.setVisibility(View.GONE);
    }

    @OnClick(R.id.number_edit_view_one)
    public void onNumberEditClickOne() {
        numberEditView.setText("1");
        onNumberEditClick();
    }

    @OnClick(R.id.number_edit_view_two)
    public void onNumberEditClickTwo() {
        numberEditView.setText("2");
        onNumberEditClick();
    }

    @OnClick(R.id.number_edit_view_three)
    public void onNumberEditClickThree() {
        numberEditView.setText("3");
        onNumberEditClick();
    }

    @OnClick(R.id.number_edit_view_four)
    public void onNumberEditClickFour() {
        numberEditView.setText("4");
        onNumberEditClick();
    }


    @OnClick(R.id.number_edit_view_five)
    public void onNumberEditClickFive() {
        numberEditView.setText("5");
        onNumberEditClick();
    }


    @OnClick(R.id.number_edit_view_six)
    public void onNumberEditClickSix() {
        numberEditView.setText("6");
        onNumberEditClick();
    }

    @OnClick(R.id.number_edit_view_seven)
    public void onNumberEditClickSeven() {
        numberEditView.setText("7");
        onNumberEditClick();
    }



    @OnClick(R.id.day_edit_view_one)
    public void onDayEditClickOne() {
        dayEditView.setText("1");
        onDayNumberEditClick();
    }

    @OnClick(R.id.day_edit_view_two)
    public void onDayEditClickTwo() {
        dayEditView.setText("2");
        onDayNumberEditClick();
    }

    @OnClick(R.id.day_edit_view_three)
    public void onDayEditClickThree() {
        dayEditView.setText("3");
        onDayNumberEditClick();
    }

    @OnClick(R.id.day_edit_view_four)
    public void onDayEditClickFour() {
        dayEditView.setText("4");
        onDayNumberEditClick();
    }


    @OnClick(R.id.day_edit_view_five)
    public void onDayEditClickFive() {
        dayEditView.setText("5");
        onDayNumberEditClick();
    }


    @OnClick(R.id.day_edit_view_six)
    public void onDayEditClickSix() {
        dayEditView.setText("6");
        onDayNumberEditClick();
    }

    @OnClick(R.id.day_edit_view_seven)
    public void onDayEditClickSeven() {
        dayEditView.setText("7");
        onDayNumberEditClick();
    }


    @OnClick(R.id.building_title)
    public void onBuildingContentClick() {
        if(buildingContent.getVisibility() == View.GONE) {
            Animation animationIn = AnimationUtils.loadAnimation(getActivity(), R.anim.abc_slide_in_top);
            buildingContent.setAnimation(animationIn);
            buildingContent.setVisibility(View.VISIBLE);
        }
        else {
           closeBuildingContent();
        }
    }

    @OnClick(R.id.building_central)
    public void onBuildingCentralClick() {
       contentValues.put(LessonContract.BUILDING, getString(R.string.building_central));
       buildingTitle.setText(R.string.building_central);
        closeBuildingContent();
    }

    @OnClick(R.id.building_mechanical)
    public void onBuildingMechanicalClick() {
        contentValues.put(LessonContract.BUILDING, getString(R.string.building_mechanical));
        buildingTitle.setText(R.string.building_mechanical);
        closeBuildingContent();
    }

    @OnClick(R.id.building_tor)
    public void onBuildingTorClick() {
        contentValues.put(LessonContract.BUILDING, getString(R.string.building_tor));
        buildingTitle.setText(R.string.building_tor);
        closeBuildingContent();
    }

    protected void closeBuildingContent() {
        Animation animationOut = AnimationUtils.loadAnimation(getActivity(), R.anim.abc_slide_out_top);
        buildingContent.setAnimation(animationOut);
        buildingContent.setVisibility(View.GONE);
    }

    @OnClick(R.id.weak_title)
    public void onWeakTitleClick() {
        if(weakContent.getVisibility() == View.GONE) {
            Animation animationIn = AnimationUtils.loadAnimation(getActivity(), R.anim.abc_slide_in_top);
            weakContent.setAnimation(animationIn);
            weakContent.setVisibility(View.VISIBLE);
        }
        else {
            closeWeakContent();
        }
    }

    @OnClick(R.id.weak_numerator_view)
    public void onWeakNumeratorClick() {
        contentValues.put(LessonContract.TYPE_OF_WEAK, Lesson.NUMERATOR);
        weakTitle.setText(R.string.weak_numerator);
        closeWeakContent();
    }

    @OnClick(R.id.weak_denumerator)
    public void onWeakDenumClick() {
        contentValues.put(LessonContract.TYPE_OF_WEAK, Lesson.DENOMINATOR);
        weakTitle.setText(R.string.weak_denumerator);
        closeWeakContent();
    }

    @OnClick(R.id.weak_every)
    public void onWeakEveryClick() {
        contentValues.put(LessonContract.TYPE_OF_WEAK, Lesson.EVERY);
        weakTitle.setText(R.string.weak_every);
        closeWeakContent();
    }

    protected void closeWeakContent() {
        Animation animationOut = AnimationUtils.loadAnimation(getActivity(), R.anim.abc_slide_out_top);
        weakContent.setAnimation(animationOut);
        weakContent.setVisibility(View.GONE);
    }

    protected void dateFromEditViewsToContentValues() {
        contentValues.put(LessonContract.NUMBER, Integer.parseInt(numberEditView.getText().toString()));
        int dayOfWeak =  Integer.parseInt(dayEditView.getText().toString());
        dayOfWeak--;
        contentValues.put(LessonContract.DAY_OF_WEAK, dayOfWeak);

        String comment = commentEditField.getText().toString();
       // if(!TextUtils.isEmpty(comment)) {
            contentValues.put(LessonContract.COMMENT, commentEditField.getText().toString());
       // }
        String teachersName = teachersNameEditField.getText().toString();
       // if(!TextUtils.isEmpty(teachersName)) {
            contentValues.put(LessonContract.TEACHERS_NAME, teachersName);
       /// }
        String flat = auditoriumEditField.getText().toString();
        //if(!TextUtils.isEmpty(flat)) {
            contentValues.put(LessonContract.FLAT, flat);
        //}
        String nameOfLesson = lessonEditField.getText().toString();
        //if(!TextUtils.isEmpty(nameOfLesson)) {
            contentValues.put(LessonContract.NAME_OF_LESSON, nameOfLesson);
       // }
    }

    public void saveDateToSQLite() {
        if(!contentValues.containsKey(LessonContract.TYPE_OF_WEAK)) {
            showLongToast(getString(R.string.choose_type_of_weak));
            return;
        }
         dateFromEditViewsToContentValues();
         getActivity().getContentResolver().insert(LessonContract.CONTENT_URI, contentValues);
         showLongToast(getString(R.string.success));
    }



}
