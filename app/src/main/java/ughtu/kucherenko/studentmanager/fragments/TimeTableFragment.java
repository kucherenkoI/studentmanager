package ughtu.kucherenko.studentmanager.fragments;

import android.content.Context;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import butterknife.ButterKnife;
import butterknife.InjectView;
import ughtu.kucherenko.studentmanager.R;
import ughtu.kucherenko.studentmanager.adapter.TimeTablePagerAdapter;
import ughtu.kucherenko.studentmanager.providers.LessonContract;

import static android.content.SharedPreferences.Editor;

/**
 * Created by kucherenko on 16.02.15.
 */
public class TimeTableFragment extends BaseFragment {

    private static final String TAG = TimeTableFragment.class.getSimpleName();

    @InjectView(R.id.view_pager)
    protected ViewPager viewPager;
    protected TimeTablePagerAdapter adapter;
    protected Cursor mCursor;
    protected SharedPreferences sharedPreferences;


    public static TimeTableFragment newInstance() {
        final TimeTableFragment fragment = new TimeTableFragment();
        return fragment;
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        adapter = new TimeTablePagerAdapter(getActivity());
        sharedPreferences = getActivity().getSharedPreferences(TAG, Context.MODE_PRIVATE);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.fragment_time_table, container, false);
        ButterKnife.inject(this, view);
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        setTypeOfWeak(2);
        adapter.setCursor(mCursor);
        viewPager.setAdapter(adapter);
        int position = sharedPreferences.getInt(LessonContract.DAY_OF_WEAK, 0);
        viewPager.setCurrentItem(position);

        Log.d(TAG, "Item count:" + String.valueOf(adapter.getCount()));
    }

    public void setTypeOfWeak(int typeOfWeak) {

        mCursor = getActivity().getContentResolver().query(LessonContract.CONTENT_URI,
                null,
                null,
                null,
                null);


        adapter.setCursor(mCursor);
    }

    @Override
    public void onPause() {
        super.onPause();
        Editor editor = sharedPreferences.edit();
        editor.putInt(LessonContract.DAY_OF_WEAK, viewPager.getCurrentItem());
        editor.commit();
    }
}
