package ughtu.kucherenko.studentmanager.fragments;

import android.content.ContentUris;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.View;

import ughtu.kucherenko.studentmanager.R;
import ughtu.kucherenko.studentmanager.pojo.Lesson;
import ughtu.kucherenko.studentmanager.providers.LessonContract;

import static ughtu.kucherenko.studentmanager.EditLessonActivity.EXTRA_LESSON_ID;

/**
 * Created by kucherenko on 03.02.15.
 */
public class EditLessonFragment extends CreateLessonFragment {

    private static final String TAG = EditLessonFragment.class.getSimpleName();
    protected int lessonId;

    public static EditLessonFragment newInstance(int lessonId) {
        final EditLessonFragment fragment = new EditLessonFragment();
        Bundle args = new Bundle();
        args.putInt(EXTRA_LESSON_ID, lessonId);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        lessonId = getArguments().getInt(EXTRA_LESSON_ID);
        Log.d(TAG, "onCreate");
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        Uri uri = ContentUris.withAppendedId(LessonContract.CONTENT_URI, lessonId);
        Cursor cursor = getActivity().getContentResolver().query(uri, null, null, null, null);
        if(cursor.moveToFirst()) {
            int number = cursor.getInt(cursor.getColumnIndex(LessonContract.NUMBER));
            numberEditView.setText(String.valueOf(number));
            int dayOfWeak = cursor.getInt(cursor.getColumnIndex(LessonContract.DAY_OF_WEAK));
            dayOfWeak++;
            dayEditView.setText(String.valueOf(dayOfWeak));
            String lesson = cursor.getString(cursor.getColumnIndex(LessonContract.NAME_OF_LESSON));
            lessonEditField.setText(lesson);
            String teacher = cursor.getString(cursor.getColumnIndex(LessonContract.TEACHERS_NAME));
            teachersNameEditField.setText(teacher);
            String flat = cursor.getString(cursor.getColumnIndex(LessonContract.FLAT));
            auditoriumEditField.setText(flat);
            int typeOfWeak = cursor.getInt(cursor.getColumnIndex(LessonContract.TYPE_OF_WEAK));
            setTypeOfWeak(typeOfWeak);
            String building = cursor.getString(cursor.getColumnIndex(LessonContract.BUILDING));

            if(building != null) {
                if (building.equals(getString(R.string.building_central))) {
                    onBuildingCentralClick();
                } else if (building.equals(getString(R.string.building_mechanical))) {
                    onBuildingMechanicalClick();
                } else if (building.equals(getString(R.string.building_tor))) {
                    onBuildingTorClick();
                }
            }

            String comment = cursor.getString(cursor.getColumnIndex(LessonContract.COMMENT));
            commentEditField.setText(comment);

        }
    }

    protected void setTypeOfWeak(int typeOfWeak) {
        switch (typeOfWeak) {
            case Lesson.NUMERATOR:
                onWeakNumeratorClick();
                break;
            case Lesson.DENOMINATOR:
                onWeakDenumClick();
                break;
            case Lesson.EVERY:
                onWeakEveryClick();
                break;
        }
    }

    @Override
    public void saveDateToSQLite() {
        dateFromEditViewsToContentValues();
        Uri uri = ContentUris.withAppendedId(LessonContract.CONTENT_URI, lessonId);
        int cnt = getActivity().getContentResolver().update(uri, contentValues, null, null);
        Log.d(TAG, "update, count = " + cnt);
        showLongToast(getString(R.string.success));
    }

}
