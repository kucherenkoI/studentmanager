package ughtu.kucherenko.studentmanager.fragments;

import android.database.Cursor;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.widget.Toast;

import ughtu.kucherenko.studentmanager.R;

public class BaseFragment extends Fragment {

    public void startFragment(Fragment fragment, Fragment back) {
        startFragment(R.id.fragment_container, fragment,
                (back != null), (back == null ? null : back.getClass().getSimpleName()),
                0, 0, 0, 0);
    }

    protected void startFragment(int resId, Fragment fragment,
                                 boolean addToStack, String stackTag,
                                 int inAnimation, int outAnimation,
                                 int pushAnimation, int popAnimation) {
        final FragmentTransaction ft = getFragmentManager().beginTransaction();
        ft.setCustomAnimations(inAnimation, outAnimation, pushAnimation, popAnimation);
        ft.replace(resId, fragment);
        ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
        if ( addToStack ) {
            ft.addToBackStack(stackTag);
        }
        ft.commit();
    }

    protected String getStringFromCursor(Cursor cursor, String columnName) {
        return cursor.getString(cursor.getColumnIndex(columnName));
    }

    protected void showLongToast(String text) {
        Toast.makeText(getActivity(), text, Toast.LENGTH_LONG).show();
    }

    protected String[] getStringArray(int res) {
        return getResources().getStringArray(res);
    }


}