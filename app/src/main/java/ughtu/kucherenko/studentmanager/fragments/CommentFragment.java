package ughtu.kucherenko.studentmanager.fragments;

import android.database.Cursor;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.RelativeLayout;

import butterknife.ButterKnife;
import butterknife.InjectView;
import ughtu.kucherenko.studentmanager.DetailCommentActivity;
import ughtu.kucherenko.studentmanager.R;
import ughtu.kucherenko.studentmanager.adapter.CommentListAdapter;
import ughtu.kucherenko.studentmanager.providers.LessonContract;
import ughtu.kucherenko.studentmanager.view.CommentItemView;

import static android.widget.AdapterView.OnItemClickListener;

/**
 * Created by kucherenko on 16.02.15.
 */
public class CommentFragment extends BaseFragment {

    @InjectView(R.id.comment_list_view)
    protected ListView commentListView;
    protected CommentListAdapter commentListAdapter;
    @InjectView(R.id.comment_empty_view)
    protected RelativeLayout commentEmptyView;

    public static CommentFragment newInstance() {
        final CommentFragment fragment = new CommentFragment();
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.fragment_comment, container, false);
        ButterKnife.inject(this, view);
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        commentListAdapter = new CommentListAdapter(getActivity());
        Cursor cursor = getCommentCursor();
        if(cursor.moveToFirst()) {
            commentListAdapter.setCursor(cursor);
            commentListView.setAdapter(commentListAdapter);
            commentListView.setOnItemClickListener(onItemClickListener);
        } else {
            commentEmptyView.setVisibility(View.VISIBLE);
        }
    }

    private Cursor getCommentCursor() {
       return getActivity().getContentResolver().query(LessonContract.CONTENT_URI,
                null,
                LessonContract.COMMENT + " IS NOT NULL AND " + LessonContract.COMMENT + " !=?",
                new String[]{""},
                null);
    }

    private OnItemClickListener onItemClickListener = new OnItemClickListener() {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            CommentItemView commentItemView = (CommentItemView) view;
            startActivity(DetailCommentActivity.newInstance(getActivity(), commentItemView.getId()));
        }
    };

}
