package ughtu.kucherenko.studentmanager;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.database.Cursor;
import android.os.Bundle;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.Toolbar;
import android.view.Gravity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.SpinnerAdapter;
import android.widget.TextView;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;
import ughtu.kucherenko.studentmanager.fragments.CommentFragment;
import ughtu.kucherenko.studentmanager.fragments.TimeTableFragment;
import ughtu.kucherenko.studentmanager.pojo.Lesson;
import ughtu.kucherenko.studentmanager.providers.LessonContract;

import static android.content.SharedPreferences.Editor;

/**
 * Created by kucherenko on 31.01.15.
 */
public class MainActivity extends BaseActivity {

    public static final String TAG =  MainActivity.class.getSimpleName();


    @InjectView(R.id.toolbar)
    protected Toolbar toolBar;
    @InjectView(R.id.tool_bar_tittle)
    protected TextView toolBarTitle;
    @InjectView(R.id.drawer_layout)
    protected DrawerLayout mDrawerLayout;
    private ActionBarDrawerToggle toggle;
    @InjectView(R.id.spinner_nav)
    protected Spinner mSpinner;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.inject(this);
        setSupportActionBar(toolBar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);


        if(savedInstanceState == null) {
            startFragment(TimeTableFragment.newInstance());
        }

        SpinnerAdapter spinnerAdapter = ArrayAdapter.createFromResource(this,
                R.array.weaks, R.layout.spinner_item_view);

        mSpinner.setAdapter(spinnerAdapter);


        final SharedPreferences sharedPreferences = getSharedPreferences(TAG, MODE_PRIVATE);
        if(sharedPreferences.contains(LessonContract.TYPE_OF_WEAK)) {
            int position = sharedPreferences.getInt(LessonContract.TYPE_OF_WEAK, 0);
            position--;
            mSpinner.setSelection(position);
        }


        mSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {



            @Override
            public void onItemSelected(AdapterView<?> adapter, View v,
                                       int position, long id) {


                Editor editor = sharedPreferences.edit();

                switch(position) {
                    case 0:
                        editor.putInt(LessonContract.TYPE_OF_WEAK, Lesson.NUMERATOR);
                        editor.commit();
                        break;
                    case 1:
                        editor.putInt(LessonContract.TYPE_OF_WEAK, Lesson.DENOMINATOR);
                        editor.commit();
                        break;

                }
                startFragment(TimeTableFragment.newInstance());
            }

            @Override
            public void onNothingSelected(AdapterView<?> arg0) {
                // TODO Auto-generated method stub

            }
        });

        toggle = new ActionBarDrawerToggle(
                this,
                mDrawerLayout,
                toolBar,
                R.string.day_of_weak,
                R.string.choose_your_group);
        toggle.setDrawerIndicatorEnabled(true);
        mDrawerLayout.setDrawerListener(toggle);

        toggle.setToolbarNavigationClickListener(onNavigationClick);

        if(isLessonDBEmpty()) {
            startActivity(CreateLessonActivity.newInstance(this));
        }

    }

    @Override
    protected void onPause() {
        super.onPause();
        startFragment(TimeTableFragment.newInstance());
        mSpinner.setVisibility(View.VISIBLE);
        toolBarTitle.setVisibility(View.GONE);
        mDrawerLayout.closeDrawer(Gravity.LEFT);
    }

    @Override
    protected void onResume() {
        super.onResume();
        startFragment(TimeTableFragment.newInstance());
    }

    public static Intent newInstance(Context context) {
        Intent intent = new Intent(context, MainActivity.class);
        return intent;
    }

    protected boolean isLessonDBEmpty() {
        Cursor cursor = getContentResolver().query(LessonContract.CONTENT_URI,
                null,
                null,
                null,
                null);
        final int zero = 0;
        if(cursor == null) {
            cursor.close();
            return true;
        }

        boolean isEmpty = cursor.getCount() == zero;
        cursor.close();
        return isEmpty;

    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        toggle.syncState();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        toggle.onConfigurationChanged(newConfig);
    }

    private View.OnClickListener onNavigationClick = new View.OnClickListener() {
        @Override
        public void onClick(View v) {


        }
    };

    @OnClick(R.id.time_table_button)
    public void onTimeTableClick() {
        startFragment(TimeTableFragment.newInstance());
        mSpinner.setVisibility(View.VISIBLE);
        toolBarTitle.setVisibility(View.GONE);
        mDrawerLayout.closeDrawer(Gravity.LEFT);
    }

    @OnClick(R.id.comment_button)
    public void onCommentClick() {
        startFragment(CommentFragment.newInstance());
        mSpinner.setVisibility(View.GONE);
        toolBarTitle.setText("Заметки");
        toolBarTitle.setVisibility(View.VISIBLE);
        mDrawerLayout.closeDrawer(Gravity.LEFT);
    }

    @OnClick(R.id.exit_button)
    public void onExitClick() {
        finish();
    }

}
