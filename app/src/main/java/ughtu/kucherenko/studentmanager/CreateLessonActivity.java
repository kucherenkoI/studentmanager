package ughtu.kucherenko.studentmanager;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;

import ughtu.kucherenko.studentmanager.fragments.CreateLessonFragment;

/**
 * Created by kucherenko on 31.01.15.
 */
public class CreateLessonActivity extends ToolBarActivity {

    protected CreateLessonFragment createLessonFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if(savedInstanceState == null) {
            createLessonFragment = CreateLessonFragment.newInstance();
            startFragment(createLessonFragment);
            toolBar.setOnMenuItemClickListener(onMenuItemClickListener);
            toolBar.setNavigationIcon(R.drawable.icon_white_left_arrow);
            setToolBarTitle(R.string.create_lesson);
        }
    }

    public static Intent newInstance(Context context) {
        Intent intent = new Intent(context, CreateLessonActivity.class);
        return intent;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_create_lesson, menu);
        return super.onCreateOptionsMenu(menu);
    }

    protected Toolbar.OnMenuItemClickListener onMenuItemClickListener = new Toolbar.OnMenuItemClickListener() {
        @Override
        public boolean onMenuItemClick(MenuItem item) {

            switch (item.getItemId()) {
                case R.id.action_save:
                    createLessonFragment.saveDateToSQLite();
                    return true;
            }
            return false;
        }
    };

}
