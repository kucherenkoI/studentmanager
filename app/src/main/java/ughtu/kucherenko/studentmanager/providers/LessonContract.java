package ughtu.kucherenko.studentmanager.providers;

import android.net.Uri;

import com.tjeannin.provigen.ProviGenBaseContract;
import com.tjeannin.provigen.annotation.Column;
import com.tjeannin.provigen.annotation.ContentUri;

import ughtu.kucherenko.studentmanager.BuildConfig;
import ughtu.kucherenko.studentmanager.pojo.Day;
import ughtu.kucherenko.studentmanager.pojo.Lesson;

import static com.tjeannin.provigen.annotation.Column.Type.INTEGER;
import static com.tjeannin.provigen.annotation.Column.Type.TEXT;

/**
 * Created by kucherenko on 31.01.15.
 */
public interface LessonContract extends ProviGenBaseContract {

    @Column(INTEGER)
    public static final String DAY_OF_WEAK = Day.FIELD_DAY_OF_WEAK;

    @Column(INTEGER)
    public static final String TYPE_OF_WEAK = Lesson.FIELD_TYPE_OF_WEAK;

    @Column(TEXT)
    public static final String TEACHERS_NAME = Lesson.FIELD_NAME_OF_TEACHER;

    @Column(TEXT)
    public static final String NAME_OF_LESSON = Lesson.FIELD_NAME_OF_LESSON;

    @Column(INTEGER)
    public static final String NUMBER = Lesson.FIELD_NUMBER;

    @Column(TEXT)
    public static final String FLAT = Lesson.FIELD_FLAT;

    @Column(TEXT)
    public static final String BUILDING = Lesson.FIELD_BUILDING;

    @Column(TEXT)
    public static final String COMMENT = "comment";


    @ContentUri
    public static final Uri CONTENT_URI = Uri.parse("content://" +  BuildConfig.APPLICATION_ID+ "/table_lessons");


}
