package ughtu.kucherenko.studentmanager.providers;

import android.content.Context;
import android.database.sqlite.SQLiteOpenHelper;

import com.tjeannin.provigen.ProviGenOpenHelper;
import com.tjeannin.provigen.ProviGenProvider;

/**
 * Created by kucherenko on 31.01.15.
 */
public class StudentContentProvider extends ProviGenProvider {

    private static Class[] contracts = new Class[]{LessonContract.class};

    @Override
    public SQLiteOpenHelper openHelper(Context context) {
        return new ProviGenOpenHelper(getContext(), "dbName", null, 1, contracts);
    }

    @Override
    public Class[] contractClasses() {
        return contracts;
    }
}
