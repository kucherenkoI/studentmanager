package ughtu.kucherenko.studentmanager.providers;

/**
 * Created by kucherenko on 18.02.15.
 */
public enum Weak {

    MONDAY(0),
    TUESDAY(1),
    WEDNESDAY(2),
    THURSDAY(3),
    FRIDAY(4),
    SATURDAY(5),
    SUNDAY(6);

    private final int index;

    Weak(int index) {
        this.index = index;
    }

    public int getIndex() {
        return index;
    }
}