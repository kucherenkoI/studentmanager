package ughtu.kucherenko.studentmanager.providers;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

/**
 * Created by kucherenko on 18.02.15.
 */
public class DateProvider {
    private static final Locale ruLocale =  new Locale("ru");
    private static final Calendar calendar = Calendar.getInstance(ruLocale);
    private static final SimpleDateFormat sdf = new SimpleDateFormat("dd MMMMM yyyy г.", ruLocale);

    public static String getFormatDate(int indexOfWeak, Weak dayOfWeak) {
        int valueForAddDay = calculateDifferent(dayOfWeak);
        calendar.add(Calendar.WEEK_OF_YEAR, indexOfWeak);
        calendar.add(Calendar.DAY_OF_MONTH, valueForAddDay);
        return sdf.format(calendar.getTime());
    }

    private static int calculateDifferent(Weak dayOfWeak) {
        int different = 0;
        int currentDay = getCurrentDayOfWeak();
        if(currentDay > dayOfWeak.getIndex())
            different = dayOfWeak.getIndex() - currentDay;
        else
        if(currentDay == dayOfWeak.getIndex())
            different = 0;
        else
        if(currentDay < dayOfWeak.getIndex())
            different = dayOfWeak.getIndex() - currentDay;
        return different;
    }

    private static int getCurrentDayOfWeak() {
        int currentDay = 0;
        switch(calendar.get(Calendar.DAY_OF_WEEK)) {
            case Calendar.MONDAY:
                currentDay = Weak.MONDAY.getIndex();
                break;
            case Calendar.TUESDAY:
                currentDay = Weak.TUESDAY.getIndex();
                break;
            case Calendar.WEDNESDAY:
                currentDay = Weak.WEDNESDAY.getIndex();
                break;
            case Calendar.THURSDAY:
                currentDay = Weak.THURSDAY.getIndex();
                break;
            case Calendar.FRIDAY:
                currentDay = Weak.FRIDAY.getIndex();
                break;
            case Calendar.SATURDAY:
                currentDay = Weak.SATURDAY.getIndex();
                break;
            case Calendar.SUNDAY:
                currentDay = Weak.SUNDAY.getIndex();
                break;
        }
        return currentDay;
    }
}
