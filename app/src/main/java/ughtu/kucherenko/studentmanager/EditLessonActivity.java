package ughtu.kucherenko.studentmanager;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;

import ughtu.kucherenko.studentmanager.fragments.EditLessonFragment;

/**
 * Created by kucherenko on 03.02.15.
 */
public class EditLessonActivity extends CreateLessonActivity {

    public static final String EXTRA_LESSON_ID = "lessonId";
    private static final String TAG = EditLessonActivity.class.getSimpleName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (savedInstanceState == null) {
            Log.d(TAG, "onCreate");
            createLessonFragment = EditLessonFragment.newInstance(getIntent().getIntExtra(EXTRA_LESSON_ID, -1));
            startFragment(createLessonFragment);
            toolBar.setOnMenuItemClickListener(onMenuItemClickListener);
            toolBar.setNavigationIcon(R.drawable.icon_white_left_arrow);
            setToolBarTitle(R.string.edit_lesson);
        }
    }

    public static Intent newInstance(Context context, int lessonId) {
        Intent intent = new Intent(context, EditLessonActivity.class);
        intent.putExtra(EXTRA_LESSON_ID, lessonId);
        return intent;
    }

}
